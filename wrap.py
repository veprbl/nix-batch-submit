#!/usr/bin/env python3

import os
import time
import json

import sys; sys.path.append('@REDIS@')
import redis

from secret import SECRET

token = os.path.basename(os.environ['out'])
builder = os.environ['__builder']
env = dict(os.environ)
del env['__builder']

r = redis.StrictRedis(host='localhost', port=6379, db=0, password=SECRET)

p = r.pubsub(ignore_subscribe_messages=True)
p.subscribe(token)
job = {
    'token' : token,
    'builder' : builder,
    'argv' : sys.argv[1:],
    'env' : env,
}
r.lpush('to_schedule', json.dumps(job))

while 1:
    msg = p.get_message()
    if msg:
        job_state = r.get(token)
        job = json.loads(job_state.decode())
        if job['status'] == 'completed':
            print(job['output'], end="")
            time.sleep(5)
            sys.exit(int(job['returncode']))
    time.sleep(5)
