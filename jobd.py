import json
import os
import random
import shutil
import stat
import subprocess
import sys
import tempfile
import time

import redis

try:
    import pyslurm
    PYSLURM_AVAILABLE = True
except ImportError:
    PYSLURM_AVAILABLE = False

try:
    import htcondor
    schedd = htcondor.Schedd()
except ImportError:
    pass

from secret import SECRET

def dict_override(d, key, val):
    if key in d:
        d[key] = val

BACKEND_SLURM = 'slurm'
BACKEND_DRMAA = 'drmaa'
BACKEND_HTCONDOR = 'htcondor'
BACKEND = BACKEND_HTCONDOR

JOB_STATUS_NEW = 'new'
JOB_STATUS_SCHEDULED = 'scheduled'
JOB_STATUS_COMPLETED = 'completed'
JOB_STATUS_CLEANUP = 'cleanup'

class Job:
    def __getattr__(self, name):
        if name in self.state:
            return self.state[name]
        else:
            raise AttributeError(name)

    def __setattr__(self, name, value):
        if name not in ['r', 'state']:
            self.state[name] = value
        else:
            super(Job, self).__setattr__(name, value)

    def __init__(self, state, r):
        self.state = state
        self.r = r

    def __repr__(self):
        return "Job(\"{}\", {})".format(self.token, self.state)

    def init(self, backend):
        """
        Initialize a new job
        """
        self.backend = backend
        # I get weird collisions in mkdtemp
        alphabet = list(map(chr, range(ord('A'), ord('Z') + 1)))
        extra_random = "".join(random.sample(alphabet, 10))
        self.tempdir = tempfile.mkdtemp(suffix=extra_random)
        print(f"init tempdir at {self.tempdir}")
        dict_override(self.env, 'NIX_BUILD_CORES', "1")
        dict_override(self.env, 'NIX_BUILD_TOP', self.tempdir)
        dict_override(self.env, 'TMP', self.tempdir)
        dict_override(self.env, 'TEMP', self.tempdir)
        dict_override(self.env, 'TMPDIR', self.tempdir)
        dict_override(self.env, 'TEMPDIR', self.tempdir)
        self.mk_wrapper()
        self.status = JOB_STATUS_NEW
        self.commit()

    def work(self):
        if self.status == JOB_STATUS_NEW:
            print("Found {} in state 'new'. Trying to shedule.".format(self.token))
            self.submit()
        elif self.status == JOB_STATUS_SCHEDULED:
            self.update_status()
        elif self.status == JOB_STATUS_COMPLETED:
            pass
        elif self.status == JOB_STATUS_CLEANUP:
            self.cleanup()

    def cleanup(self):
        if job.status not in [JOB_STATUS_NEW, JOB_STATUS_COMPLETED]:
            self.cancel()
        print("Cleaning up job {}".format(self.token))
        def unlink(filename):
            try:
                os.unlink(filename)
            except FileNotFoundError:
                pass
        unlink(self.env_file)
        unlink(self.wrapper_file)
        if 'output_file' in self.state:
            unlink(self.output_file)
        shutil.rmtree(self.tempdir, ignore_errors=True)
        self.r.lrem('jobs', 0, self.token)
        self.r.delete(self.token)
        self.state = None

    def commit(self):
        self.r.set(self.token, json.dumps(self.state))
        num_subscribers = self.r.publish(self.token, "awesome data")
        assert num_subscribers <= 1

    def mk_wrapper(self):
        """
        Creates wrapper files needed to transfer environment variables directly
        to the job.  This is needed, for example, because HTCondor is not able
        to transfer some of the (multiline?) environment
        """
        with tempfile.NamedTemporaryFile(mode="w", delete=False) as fp:
            self.env_file = fp.name
            json.dump(self.env, fp)
            fp.flush()
        with tempfile.NamedTemporaryFile(mode="w", delete=False) as fp:
            self.wrapper_file = fp.name
            fp.write("""\
#!{python}
import sys, os
import json
# Preseve variables set by condor
env = dict(os.environ)
with open("{env_file}", "r") as fp:
    new_env = json.load(fp)
env.update(new_env)
if "_CONDOR_SCRATCH_DIR" in env:
    scratch_dir = env["_CONDOR_SCRATCH_DIR"]
    top_dir = os.path.join(scratch_dir, "top")
    temp_dir = os.path.join(scratch_dir, "temp")
    os.mkdir(top_dir)
    os.mkdir(temp_dir)
    env["NIX_BUILD_TOP"] = top_dir
    env["TMP"] = temp_dir
    env["TEMP"] = temp_dir
    env["TMPDIR"] = temp_dir
    env["TEMPDIR"] = temp_dir
os.chdir(env["NIX_BUILD_TOP"])
os.execve(sys.argv[1], sys.argv[1:], env)
""".format(python=sys.executable, env_file=self.env_file))
            fp.flush()
            os.chmod(self.wrapper_file, stat.S_IRUSR | stat.S_IXUSR) # needed for htcondor

    def submit(self):
        if self.backend == BACKEND_HTCONDOR:
            self.htcondor_submit()
        elif self.backend == BACKEND_DRMAA:
            self.drmaa_submit()
        elif self.backend == BACKEND_SLURM:
            self.slurm_submit()
        else:
            raise ValueError(f"Unknown backend '{self.backend}'")

    def htcondor_submit(self):
        self.output_file = tempfile.NamedTemporaryFile(delete=False).name
        sub = htcondor.Submit({
            'Universe': "vanilla",
            'Executable': self.wrapper_file,
            'Arguments': " ".join([self.builder] + self.argv),
            'Output': self.output_file,
            'Error': self.output_file,
            'request_memory': self.env.get('__htcondor_request_memory', "1G"),
            'description': self.env.get('name', ""),
        })
        while True:
            try:
                with schedd.transaction() as txn:
                    self.job_id = sub.queue(txn)
                    break
            except RuntimeError as e:
                print("Exception raised while trying to submit a job:")
                print(e)
                time.sleep(60)
        self.status = JOB_STATUS_SCHEDULED
        self.commit()

    def drmaa_submit(self):
        self.output_file = tempfile.NamedTemporaryFile(delete=False).name
        job_template = drmaa_session.createJobTemplate()
        job_template.remoteCommand = self.wrapper_file
        job_template.args = [self.builder] + self.argv
        job_template.outputPath = f":{self.output_file}"
        job_template.joinFiles = True
        self.job_id = drmaa_session.runJob(job_template)
        drmaa_session.deleteJobTemplate(job_template)
        self.status = JOB_STATUS_SCHEDULED
        self.commit()

    def slurm_submit(self):
        self.output_file = tempfile.NamedTemporaryFile(delete=False).name
        job_name = self.env.get('name', "nameless")
        try:
            with subprocess.Popen([
                    "sbatch",
                    "--job-name", job_name,
                    "--export", "NONE", # environment vars are set by the wrapper
                    "--output", self.output_file,
                    "--time", self.env.get('__slurm_job_timelimit', "1-0"), # default is 1 day
                    #"-p", "shared", "--image=custom:pdsf-chos-sl64:v4",
                    self.wrapper_file, self.builder] + self.argv,
                    stdout=subprocess.PIPE, stderr=sys.stderr) as proc:
                proc.wait()
                outs = proc.stdout.read().decode()
                retries = self.state.get('retries', 0)
                if outs.find("Submitted batch job ") == -1:
                    assert(retries < 3)
                    self.retries = retries + 1
                    self.commit()
                    print("Failed to submit slurm job")
                    print(outs)
                    return
                outs = outs.splitlines()[0]
                assert outs.startswith("Submitted batch job ")
                self.slurm_job_id = int(outs[len("Submitted batch job "):])
                if proc.returncode != 0:
                    sys.stdout.buffer.write(job_output_file.read())
                    sys.exit(proc.returncode)
        except OSError as e:
            print("Error in slurm_submit: {}".format(e))
            return
        self.status = JOB_STATUS_SCHEDULED
        self.commit()

    def update_status(self):
        if self.backend == BACKEND_HTCONDOR:
            self.htcondor_update_status()
        elif self.backend == BACKEND_DRMAA:
            self.drmaa_update_status()
        elif self.backend == BACKEND_SLURM:
            self.slurm_update_status()
        else:
            raise ValueError(f"Unknown backend '{self.backend}'")

    def htcondor_update_status(self):
        try:
            job_info = list(schedd.xquery(projection=["ProcID", "JobStatus"], requirements="ClusterId==%d" % self.job_id))
        except RuntimeError as e:
            print(e)
            return
        if job_info:
            job_info, = job_info
            return
        else:
            try:
                job_info, = list(schedd.history(requirements="ClusterId==%d" % self.job_id, projection=['JobStatus', 'ExitCode'], match=1))
            except RuntimeError as e:
                print(e)
                return
        if job_info['JobStatus'] == 3: # 3 - cancelled
            self.status = JOB_STATUS_CLEANUP
        elif job_info['JobStatus'] == 4: # 4 - complete
            self.returncode = job_info.get('ExitCode', None)
            with open(self.output_file, "r", errors='backslashreplace') as fp:
                self.output = fp.read()
            self.status = JOB_STATUS_COMPLETED
            # delay to let build product appear at "$out"
            if self.returncode == 0:
                for _ in range(100):
                    out = self.env['out']
                    if os.path.exists(out):
                        break
                    time.sleep(2)
                    print(f"Waiting for '{out}' to appear")
        else:
            print(f"Unexpected job status {job_info['JobStatus']} for {self.job_id}")
        self.commit()

    def drmaa_update_status(self):
        try:
            status = drmaa_session.jobStatus(self.job_id)
        except drmaa.errors.InvalidJobException:
            print(f"drmaa_update_status: no such job: {self.job_id}")
            if self.status == JOB_STATUS_SCHEDULED:
                self.returncode = 123
                self.status = JOB_STATUS_COMPLETED
                self.commit()
            return
        if status in [drmaa.JobState.DONE, drmaa.JobState.FAILED]:
            retval = drmaa_session.wait(self.job_id, drmaa.Session.TIMEOUT_NO_WAIT)
            print(retval)
            self.returncode = retval.exitStatus
            self.status = JOB_STATUS_COMPLETED
            # delay to let build product appear at "$out"
            if self.returncode == 0:
                for _ in range(10):
                    out = self.env['out']
                    if os.path.exists(out):
                        break
                    time.sleep(2)
                    print(f"Waiting for '{out}' to appear")
            with open(self.output_file, "r") as fp:
                self.output = fp.read()
            self.commit()
            print("done")

    def slurm_update_status(self):
        global pyslurm_jobs
        print("checking status of {}".format(self.token))
        if PYSLURM_AVAILABLE:
            if self.slurm_job_id in pyslurm_jobs:
                slurm_job_state = pyslurm_jobs[self.slurm_job_id]['job_state']
                slurm_exit_code = pyslurm_jobs[self.slurm_job_id]['exit_code']
            else:
                print("job not found")
                retries = self.state.get('retries', 0)
                if retries < 3:
                    self.retries = retries + 1
                    self.commit()
                    print("trying to reschedule")
                    self.submit()
                return
        else:
            with subprocess.Popen(["scontrol", "show", "job", str(self.slurm_job_id)],
                    stdout=subprocess.PIPE, stderr=sys.stderr) as proc:
                proc.wait()
                if proc.returncode != 0:
                    print("Could not fetch job status")
                    time.sleep(10)
                    return
                outs = proc.stdout.read().decode()
                status = outs.split()
                slurm_job_state, = filter(lambda s: s.startswith("JobState="), status)
                slurm_job_state = slurm_job_state[len("JobState="):]
                slurm_exit_code, = filter(lambda s: s.startswith("ExitCode="), status)
                slurm_exit_code = slurm_exit_code[len("ExitCode="):]
        returncode = int(slurm_exit_code.split(":")[0])
        print(slurm_job_state)
        if int(slurm_exit_code.split(":")[1]) != 0:
            print("slurm exit code {}", slurm_exit_code)
        if slurm_job_state == 'COMPLETED' or slurm_job_state == 'FAILED':
            print("Job {} is now {}".format(self.token, slurm_job_state))
            self.returncode = returncode
            self.status = JOB_STATUS_COMPLETED
            with open(self.output_file, "r") as fp:
                self.output = fp.read()
            retries = self.state.get('retries', 0)
            if self.output.find("getpwuid(): uid not found:") != -1 and retries < 3:
                self.retries = retries + 1
                self.commit()
                print("getpwuid problem:")
                print(self.output)
                self.submit()
                return
            self.commit()

    def cancel(self):
        if self.backend == BACKEND_HTCONDOR:
            self.htcondor_cancel()
        elif self.backend == BACKEND_DRMAA:
            self.drmaa_cancel()
        elif self.backend == BACKEND_SLURM:
            self.slurm_cancel()
        else:
            raise ValueError(f"Unknown backend '{self.backend}'")

    def htcondor_cancel(self):
        if "job_id" not in self.state:
            print("htcondor_cancel: job_id is not set for {self}")
            return
        schedd.act(htcondor.JobAction.Remove, f"ClusterId=={self.job_id}")

    def drmaa_cancel(self):
        try:
            drmaa_session.control(self.job_id, drmaa.JobControlAction.TERMINATE)
        except drmaa.errors.InvalidJobException:
            print(f"drmaa_cancel: no such job: {self.job_id}")

    def slurm_cancel(self):
        print("Canceling job {}".format(self.token))
        with subprocess.Popen(["scancel", str(self.slurm_job_id)]) as proc:
            proc.wait()
            if proc.returncode != 0:
                print("scancel failed!")

r = redis.StrictRedis(host='localhost', port=6379, db=0, password=SECRET)
if BACKEND == BACKEND_DRMAA:
    import drmaa
    drmaa_session = drmaa.Session()
    drmaa_session.initialize()
    # TODO call drmaa_session.exit()
while 1:
    while 1:
        item = r.rpop('to_schedule')
        if item is None:
            break
        try:
            job_spec = json.loads(item.decode())
        except Exception as e:
            print(e)
            continue
        token = job_spec['token']
        # check that there is no such job yet
        available1 = r.get(token) is not None
        available2 = token in r.lrange('jobs', 0, -1)
        if available1:
            _, n = r.pubsub_numsub(token)[0]
            print("Job {} requested by {} client(s)".format(token, n))
        # make a new job
        job = Job(job_spec, r)
        job.init(BACKEND)
        # check that job is still tracked
        _, n = r.pubsub_numsub(token)[0]
        if n == 0:
            if job.status != JOB_STATUS_COMPLETED:
                print("Nobody is waiting on {}. Wrapping up...".format(token))
            job.status = JOB_STATUS_CLEANUP
        else:
            r.rpush('jobs', job.token)
            print("Scheduling {}".format(job.token))
            job.submit()
    tokens = r.lrange('jobs', 0, -1)
    def load_job(token):
        job_state = r.get(token)
        try:
            job_state = json.loads(job_state.decode())
        except Exception as e:
            print(e)
            return None
        return Job(job_state, r)
    jobs = list(map(load_job, tokens))
    for (token, n), job in zip(r.pubsub_numsub(*tokens), jobs):
        if n == 0:
            if job.status != JOB_STATUS_COMPLETED:
                print("Nobody is waiting on {}. Wrapping up...".format(token))
            job.status = JOB_STATUS_CLEANUP
    if PYSLURM_AVAILABLE:
        pyslurm_jobs = pyslurm.job()
        if pyslurm_jobs is None:
            print("pysurm.job() returned None")
            pyslurm_jobs = {}
        else:
            try:
                pyslurm_jobs = pyslurm_jobs.get()
            except ValueError as e:
                print("Error getting slurm jobs {}".format(e))
                pyslurm_jobs = []
    for job in jobs:
        if job is None:
            continue
        job.work()
    time.sleep(100)
