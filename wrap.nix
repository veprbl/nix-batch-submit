let
  nixpkgs = import <nixpkgs> {};
  condor_wrapper = nixpkgs.stdenv.mkDerivation {
    name = "condor-wrapper";
    buildInputs = [ nixpkgs.python3 ];
    phases = [ "installPhase" "fixupPhase" ];
    installPhase = ''
      install -Dm755 ${./wrap.py} $out/wrap.py
      install -Dm755 ${./secret.py} $out/secret.py
      substituteInPlace $out/wrap.py --subst-var-by REDIS "$(toPythonPath ${nixpkgs.python3Packages.redis})"
    '';
  };
in
  d: nixpkgs.lib.overrideDerivation d (_: {
    builder = "${condor_wrapper}/wrap.py";
    __builder = d.builder;
  })
