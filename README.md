**nix-batch-submit** provides means to define batch jobs using
[Nix](http://nixos.org/nix/) derivations. Author believes that Nix can be used
not only to do package management, but to track general computation process in
the Unix-like environment. When performing large volume of such computations
(or data processing) one wants to parallelise them. Nix already provides means
for such parallelization within a single node (```-j``` flag) or among multiple
nodes (```nix-build-remote```, Hydra). Existing computer centres will often
employ specialised batch processing system which, for example, manage resource
distribution between users and groups.

Should this become an RFC (at [NixOS/rfcs](https://github.com/NixOS/rfcs)) this
should do a better job at separating changes required to Nix ecosystem from
implementation of the interface to batch system. This is also to state that the
changes to Nix are to be as minimal and as generally useful as possible.

# Goals

 * enable users to do batch processing within existing scheduling system while employing all power of Nix
 * jobs defined via Nix expression language
 * target [HTCondor](https://research.cs.wisc.edu/htcondor/) and [Slurm](https://slurm.schedmd.com)
 * jobs should also be buildable locally without a cluster, and hashes of the output paths should remain the same
 * separation between derivations to be ran on a cluster or locally (later provides lower latency before the "build" start)
 * jobs should be terminated if user interrupts the Nix builder process

# Challenges
 * only user-level access to cluster (no root, limited resources, have to follow existing security policy)
 * shared filesystems based on NFS (Nix uses SQLite in backend), GPFS (large block size, limited retention times)
 * upstreaming changes to Nix is difficult

# Approaches
## NIX\_BUILD\_HOOK
Nix allows to specify a program to dispatch .drv realisation request to a
remote machine along with its build inputs (aka dependencies), it is also
responsible for retrieving build outputs from the builder back to the local
store.

The implementation would consist of a hook that for each built derivation would
schedule a batch job that would perform the build remotely. The issue here is
mostly that copying of the store paths is undesirable due to their large sizes.

Since store is located on a shared NFS it is tempting to skip copying
altogether and just call ```nix-store --realise``` from batch job directly on
the .drv and inputs in the store. This approach has a couple problems. Firstly,
Nix store is backed by an SQLite database that keeps track of valid store paths
as well as their interdependencies. To prevent corruption one has to set
```use-sqlite-wal = false```. This however still does not allow concurrent access.
Seems like a reliable solution to this can be achieved by adding a
backend to one of the centralised databases (upstream doesn't mind PostgreSQL
NixOS/nix#378).  Secondly, there are problems with locking where both
```nix-build``` and ```nix-store --realise``` will lock output paths in the
store, so we can't chain them. In ```nix-build-remote``` this is not a problem
because the locks happen on different stores (one of them on a remote machine)
and ```nix copy``` doesn't try to lock paths present in ```NIX_HELD_LOCKS```
environment variable. The solution to this might be to simply make
```nix-store``` obey ```NIX_HELD_LOCKS```.

A possible drawback is that ```NIX_BUILD_HOOK``` might disappear in future
versions Nix (see NixOS/nix#1221).

## Overriding derivations
The approach is to override derivation so that builder now points to a wrapper
that will schedule a batch job that will execute the real builder for the
derivation.

Preserving the ouput hashes between local/remote build mode in such scheme
would require some kind of impurity. It's not clear how to achieve this.

A major drawback is that such wrapper is to block until the job is finished, so
the number of jobs submitted to the batch system queue will be limited by the
```max-jobs``` limit in the Nix. Ideally, all jobs should be queued ASAP. If
```max-jobs``` limit is raised it can lead to starting too many of the "local
type" jobs on the interactive node and this will interfere with other users.

A minor drawback is that, when interrupted, Nix sends ```SIGKILL``` to the
builder. This can't be trapped by signal handler, so can not be terminated by
the builder. This problem might be also relevant to the ```NIX_BUILD_HOOK```
approach.

This method is also limited by Sqlite backend operation on NFS. Namely parallel
independent Nix builds are still not possible.
