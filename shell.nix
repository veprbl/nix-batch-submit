with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "jobd-shell";
  buildInputs = [
    redis
    python3
    python3Packages.redis
    python3Packages.htcondor
    ];
}
